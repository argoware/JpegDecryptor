<!--
SPDX-FileCopyrightText: 2019 Luca Bassi

SPDX-License-Identifier: GPL-3.0-or-later
-->

# JPEG decryptor for ransomware
Extracts, if present, the biggest image (`ffd8ff`...`ffd9`) in the jpeg file crypted by the ransomware (discards all metadata).
Adapted for id-..._decipher@keemail.me that crypts the first 30000 bytes.
Usage: `./decrypt.sh ./folderWithImages/`
