#!/usr/bin/env sh

# SPDX-FileCopyrightText: 2019 Luca Bassi
#
# SPDX-License-Identifier: GPL-3.0-or-later

success=0
total=0
files=$( find "$1" -type f \( -iname "*.jpg.id-8803002542_decipher@keemail.me" -o -iname "*.jpeg.id-8803002542_decipher@keemail.me" \) )
while read file
do
	echo "Decrypting $file"
	total=$((total + 1))
	value=$(xxd -c 1 "$file" | ./calculateOffset.awk)
	echo "Value $value"
	if [ "$value" == "," ]; then
		echo "Error (not found): $file"
	else
		IFS=',' read -ra values <<< "$value"
		if [ "${values[1]}" -lt 25000 ]; then
			echo "Error (too small): $file"
		else
			dd if="$file" of="${file::-34}" bs=1 skip=${values[0]} count=${values[1]}
			success=$((success + 1))
			rm "$file"
		fi
	fi
done <<< "$files"
echo "Decrypted $success/$total ($(( success * 100 / total))%)"
