#!/usr/bin/env -S awk -f

# SPDX-FileCopyrightText: 2019 Luca Bassi
#
# SPDX-License-Identifier: GPL-3.0-or-later

BEGIN {
	FS = " "
}
{
	if ($2 == "d9" && appn == 1 && ff == 1) {
		#print "End " count
		if (count-start+1 > lenght) {
			skip = start
			lenght = count-start+1
		}
		appn = 0
	}
	if ($2 ~ /^e[0-3]|db$/ && soi == 1) {
		start = count-3
		#print "Start " start
		appn = 1
	}
	soi = 0
	if ($2 == "ff" && d8 == 1) {
		soi = 1
	}
	d8 = 0
	if ($2 == "d8" && ff == 1 && count >= 30000) {
		d8 = 1
	}
	ff = 0
	if ($2 == "ff") {
		ff = 1
	}
	count++
}
END {
	print skip "," lenght
}
